const { client, xml, jid } = require("@xmpp/client")
const debug = require("@xmpp/debug")
const mongoose = require('mongoose')
const uuid = require('uuid')

// load config from .env file (if exists)
require("dotenv").config()
const TRICKBOT_USERNAME = process.env.TRICKBOT_USERNAME
const TRICKBOT_JID = jid(`${TRICKBOT_USERNAME}@${process.env.TRICKBOT_SERVICE}`)

// connect to the database

mongoose.connect(process.env.TRICKBOT_MONGODB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function() {
  console.log('connected to database.')
})

// card helpers
const SUITS = new Map([
  ['c', 'clubs'],
  ['d', 'diamonds'],
  ['h', 'hearts'],
  ['s', 'spades'],
])

const RANKS = new Map ([
  ['a', 'ace'],
  ['2', 'two'],
  ['3', 'three'],
  ['4', 'four'],
  ['5', 'five'],
  ['6', 'six'],
  ['7', 'seven'],
  ['8', 'eight'],
  ['9', 'nine'],
  ['10', 'ten'],
  ['j', 'jack'],
  ['q', 'queen'],
  ['k', 'king'],
])

const SUIT_ORDER = [...SUITS.keys()]
const RANK_ORDER = [...RANKS.keys()]

const card_re = /^(?<suit>[cdhs])(?<rank>[a23456789jqk]|10)$/i //checks if card is valid

function makeDeck() {
  var deck = []
  for (s of SUITS.keys()) {
    for (r of RANKS.keys()) {
      deck.push(`${s}${r}`)
    }
  }
  return deck
}

function shuffle(deck) {
  for (var i=0; i<1000; i++) {
    // pick two random cards
    var l1 = Math.floor(Math.random() * deck.length)
    var l2 = Math.floor(Math.random() * deck.length)

    // exchange places of those cards
    var [a,b] = [ deck[l1], deck[l2] ]
    deck[l1] = b
    deck[l2] = a
  }
  return deck
}


/*
 * Decides whether one card is greater or less
 * than another. Compares first by suit and then
 * by rank.
 *
 * returns:
 *   difference between suits if suits are different
 *   difference between ranks if ranks are different
 *   0 if both suit and rank are the same
 *
 * "difference" can be a positive or negative number, depending
 * on whether the first card is of higher or lower value than
 * the second.
 */

function compareCards(cardString1, cardString2) {
  // extract suit and rank data from cardString
  var c1 = card_re.exec(cardString1.toLowerCase())
  var c2 = card_re.exec(cardString2.toLowerCase())

  // make sure cards exist
  if (!c1) throw TypeError(`Invalid card: ${cardString1}`)
  if (!c2) throw TypeError(`Invalid card: ${cardString2}`)

  // compare by suit then rank
  c1.suitValue = SUIT_ORDER.indexOf(c1.groups.suit)
  c2.suitValue = SUIT_ORDER.indexOf(c2.groups.suit)

  // return by suit value if different
  if (c1.suitValue != c2.suitValue) {
    return c1.suitValue - c2.suitValue
  }

  // otherwise, suit must be same
  // compare by rank instead
  c1.rankValue = RANK_ORDER.indexOf(c1.groups.rank)
  c2.rankValue = RANK_ORDER.indexOf(c2.groups.rank)

  return c1.rankValue - c2.rankValue
}

// Also known as "sort cards"
function unshuffle(deck) {
  deck.sort(compareCards)
  return deck
}

// define schemas

var tableSchema = new mongoose.Schema({
  table_id: String,
  host: String, // JID of host
  users: [String], // JIDs of people in room

  playerMap: [{
    jid: String, // JID of user
    player: String // Name of player (eg. North)
  }],

  game: {
    players: {
      type: Map, // one of: North, South, East, West
      of: {
        hand: [ // list of cards
          { type: String, lowercase: true, match: card_re }
        ],
        tricks: [[ // list of [card, card] pairs
          { type: String, lowercase: true, match: card_re }
        ]],
      },
    },
    stage: {
      type: String,
      default: 'new',
      enum: ['new', 'playing', 'finished'],
    },
    move: { type: Number, default: 1 }, // cycles among players list
    trick: [ // cards currently on the table
      {
        card: { type: String, lowercase: true, match: card_re },
        owner: { type: String },
      }
    ],
    winner: { type: String, default: '' },
  }
})


// ensure that all trick "owners" are current players
tableSchema.pre('validate', function(next) {
  const players = this.game.players
  this.game.trick.forEach(function (trick, index) {
    if (!players.has(trick.owner)) {
      this.game.trick.invalidate(
        `game.trick.${index}`,
        'Trick owner must match an existing player',
        trick
      )
    }
  })

  next()
})

tableSchema.methods.userToPlayer = function (userJID) {
  var playerName

  for (const link of this.playerMap) {
    if (link.jid == userJID) {
      playerName = link.player
    }
  }

  return {
    playerName: playerName,
    player: this.game.players.get(playerName)
  }
}

tableSchema.methods.startGame = async function () {

  // set hands for each player and clear tricks
  for (const [key, player] of this.game.players.entries()) {
    player.tricks = []
    player.hand = []
  }

  // prepare deck
  var deck = shuffle(makeDeck())

  // deal cards (5 each)
  for (var i=0; i<5; i++) {
    for (const [key, player] of this.game.players.entries()) {
      player.hand.push(deck.shift())
    }
  }

  // unshuffle hands
  for (const [key, player] of this.game.players.entries()) {
    unshuffle(player.hand)
  }

  this.game.stage = 'playing'
  this.game.move = 0

  // reset winner and current trick, in case they were set
  this.game.trick = []
  this.game.winner = ''

  return {
    status: 'ok',
    message: 'Game ready',
  }
}

tableSchema.methods.showStatus = function(userJID) {
  var messages = []
  var { playerName, player } = this.userToPlayer(userJID)
  if (this.game.stage == 'new') {
    messages.push('This is a new game. Use "start" to start playing')
  } else if (this.game.stage == 'finished') {
    messages.push('Game over!')
    messages.push(`Winner: ${this.game.winner}`)
    messages.push('🎉')
  } else {
    var hand_string = player.hand.join(', ').toUpperCase()
    messages.push(`Your cards: ${hand_string}`)

    var trick_string = (this.game.trick
      .map((c)=>{ return c.card })
      .join(', ').toUpperCase())

    messages.push(`Current trick: ${trick_string}`)
  }

  return messages
}

tableSchema.methods.nextMove = async function() {
  var status, message, move, trickWinner

  if (this.game.stage == 'playing') {
    this.game.move = (this.game.move + 1) % this.game.players.size
    move = this.game.move

    // assign tricks, if necessary
    if (this.game.trick.length >= this.game.players.size) {

      // first, calculate who got the highest card
      highestCard = this.game.trick.reduce((a, b) => {
        if (compareCards(a.card, b.card) < 0) {
          return b
        } else {
          return a
        }
      })

      highestPlayer = this.game.players.get(highestCard.owner)

      // throw error if the player doesn't exist
      if (!highestPlayer) throw ValueError(`Invalid player: ${highestCard.owner}`)

      // give the trick to that player
      highestPlayer.tricks.push(this.game.trick.map((c)=>{ return c.card }))
      this.game.trick = []

      // save info
      trickWinner = highestCard.owner
      message = `${highestCard.owner} wins the trick`
    }

    return {
      status: 'ok',
      message,
      move,
      trickWinner
    }
  }
}

tableSchema.methods.getCurrentPlayer = function() {
    return this.playerMap[this.game.move]
}

tableSchema.methods.playCard = async function(userJID, card) {

  if (this.game.stage == 'new') {
    return {
      error: 'game-not-started',
      message: 'This game hasn\'t started yet!',
    }
  } else if (this.game.stage == 'finished') {
    return {
      error: 'game-over',
      message: 'This game is finished! Start a new game if you want to play.',
    }
  }

  // check if player matches
  var currentPlayer = this.getCurrentPlayer()
  console.log(`Move: ${this.game.move}`)
  if (currentPlayer.jid != userJID) {
    return {
      error: 'not-your-turn',
      message: `It's not your turn! Please wait for ${currentPlayer.jid} to finish.`,
    }
  }

  var { playerName, player } = this.userToPlayer(userJID)
  var cardIndex = player.hand.indexOf(card.toLowerCase())

  // check if the card is actually in the hand
  if (cardIndex == -1) {
    return {
      error: 'card-not-in-hand',
      message: `You don't have ${card} in your hand right now!`
    }
  }

  var cards = player.hand.splice(cardIndex, 1)
  this.game.trick.push({ card: cards[0], owner: playerName })

  var { status, trickWinner } = await this.nextMove()

  // temporary hack: if the trick is over and you don't have
  // any more cards then end the game and declare the winner

  if(trickWinner && player.hand.length == 0) {
    this.game.stage = 'finished'

    // figure out winner
    var winner
    var maxTricks = 0 // max tricks so far
    for (const [playerName, player] of this.game.players.entries()) {
      if (player.tricks.length > maxTricks) {
        winner = playerName
        maxTricks = player.tricks.length
      }
    }

    this.game.winner = winner

    return {
      status: 'ok',
      message: `${winner} wins with ${maxTricks} tricks!`,
      card: card,
      gameWinner: winner,
    }
  }

  // Let people know if someone a trick
  if (trickWinner) {
    return {
      status: 'ok',
      message: `You played ${card} and ${trickWinner} got the trick!`,
      card: card,
      trickWinner: trickWinner,
    }
  }

  // otherwise, just return a generic success message
  return {
    status: 'ok',
    message: `You played: ${card}`,
    card: card,
  }
}

var SingleTable = mongoose.model('SingleTable', tableSchema)

// mongoose helper function

async function getTableByHost(host) {
  var table = await SingleTable.findOne({ host: host })

  if (!table) {
    // set up new table
    table = new SingleTable({
      host: host,
      users: [ host, `${TRICKBOT_JID.bare()}` ],
      playerMap: [{
        jid: host,
        player: 'North',
      },
      {
        jid: `${TRICKBOT_JID.bare()}`,
        player: 'South',
      }],
      game: {
        players: {
          North: {
            hand: [],
            tricks: [],
          },
          South: {
            hand: [],
            tricks: [],
          }
        },
        stage: 'new',
      }
    })
  }

  return table
}

// xmpp helper functions

function sendMessage(xmpp, recipient, msg) {
  xmpp.send(xml(
    'message',
    { type: 'chat', to: recipient, id: uuid.v4() },
    xml('body', {}, msg),
  ))
}

// temporary bot engine


async function botPlay(table, userJID) {
  // First, get the player
  var bot = table.userToPlayer(`${TRICKBOT_JID.bare()}`).player
  console.log(table)

  if (bot && bot.hand.length > 0) {
    var botCard = bot.hand[Math.floor(Math.random()*bot.hand.length)]

    console.log(`playing: ${botCard}`)
    var { status, message, error, trickWinner } = await table.playCard(`${TRICKBOT_JID.bare()}`, botCard)

    if (error) {
      // TODO: intelligently handle error
      console.log(`bot: ${error}`)
      if (error == 'game-over') return // fail silently
      sendMessage(xmpp, userJID, `Bot got error: ${message}`)
      return
    }

    await table.save()

    sendMessage(xmpp, userJID, `Bot played: ${botCard}`)

    // declare if trick was won
    if (trickWinner) {
      sendMessage(xmpp, userJID, `${trickWinner} gets the trick!`)
    }
  }
}

// connect to the xmpp server

const xmpp = client({
  service: process.env.TRICKBOT_SERVICE,
  username: process.env.TRICKBOT_USERNAME,
  password: process.env.TRICKBOT_PASSWORD,
})

if (process.env.NODE_ENV == 'development') {
  debug(xmpp, true)
}


xmpp.on("error", (err) => {
  console.error(err)
})

xmpp.on("offline", () => {
  console.log("offline")
})

xmpp.on("stanza", async (stanza) => {

  var from = stanza.attrs.from ? jid(stanza.attrs.from) : undefined

  // ignore own messages
  if (
    from &&
    from.local == TRICKBOT_JID.local &&
    from.host == TRICKBOT_JID.host
  ) {
    console.log("ignoring own message")
    return
  }

  // handle presence
  if (stanza.is("presence")) {
    if (stanza.attrs.type == "subscribe")
    xmpp.send(xml("presence", { type: "subscribed", to: from }))
  }

  // after this, ignore non-messages
  if (!stanza.is("message")) return

  // interpret message
  var command
  var body = stanza.getChild("body")
  var message
  if (body) {
    command = stanza.getChild("body").text().split(/ +/)
    console.log(`We got command: ${command[0]}`)

    var userJID = stanza.attrs.from // JID of current user
    var table

    // process command
    switch(command[0].toLowerCase()) {


      case 'ping': {
        sendMessage(xmpp, userJID, 'pong')
        break
      }

      case 'help': {
        if (Math.floor(Math.random() * 3) > 0) {
          sendMessage(xmpp, userJID, 'Hi! I\'m trickbot. Did I introduce myself before?')
        }

        sendMessage(xmpp, userJID, 'You can use the following commands to control me: help, ping, play, restart, start, status, bot.')

        if (Math.floor(Math.random() * 4) == 0) {
          sendMessage(xmpp, userJID, 'Right now I can only play a simple single-player game where you have to play cards from your hand and no other. The game will end once you finish playing all the cards.')
          sendMessage(xmpp, userJID, 'I know it\'s a bit lonely playing on your own, but the advantage is that you\'ll always win 😉')
        }

        if (Math.floor(Math.random() * 3) == 0) {
          sendMessage(xmpp, userJID, 'I can\'t understand most other commands right now, but who knows—maybe I will someday 💭')
        }
        break
      }


      case 'start': {
        table = await getTableByHost(userJID)
        if (table.game.stage == 'new') {
          await table.startGame()
          await table.save()

          sendMessage(xmpp, userJID, 'The game begins!')
          // display status
          for (m of await table.showStatus(userJID)) {
            sendMessage(xmpp, userJID, m)
          }
          sendMessage(xmpp, userJID, 'This is your first time, right? Don\'t worry, it\'s simple. Just write "play" and the card you want to play—for example, "play H6" to play 6 of hearts')

        } else {
          sendMessage(xmpp, userJID, 'This game is already in progress. To abort it please use "restart" instead.')
        }
        break
      }

      case 'restart': {
        table = await getTableByHost(userJID)
        await table.startGame()
        await table.save()

        // let's go!
        sendMessage(xmpp, userJID, 'The game begins!')

        // display status
        for (m of await table.showStatus(userJID)) {
          sendMessage(xmpp, userJID, m)
        }
        break
      }

      case 'reset': {
        // deletes this table, to regenerate next time
        (await getTableByHost(userJID)).delete()
        sendMessage(xmpp, userJID, '/me clears the table')
        break
      }

      case 'status': {
        table = await getTableByHost(userJID)
        const messages = table.showStatus(userJID)
        for (m of messages) {
          sendMessage(xmpp, userJID, m)
        }
        break
      }

      case 'play': {
        table = await getTableByHost(userJID)
        if (command.length < 2) {
          sendMessage(xmpp, userJID, 'What card do you want to play?')
          break
        }

        var { status, message, error, trickWinner } = await table.playCard(userJID, command[1])

        if (error) {
          // TODO: intelligently handle error
          console.log(error)

          if (error == 'not-your-turn' && table.getCurrentPlayer().jid == `${TRICKBOT_JID.bare()}`) {
            sendMessage(xmpp, userJID, "It's not your turn! Please be patient.")
            sendMessage(xmpp, userJID, "If the bot's gotten stuck, you might have to type 'reset' to delete the whole table and start from scratch. (This is different from 'restart' which just clears the cards)")
            return
          }


          sendMessage(xmpp, userJID, message)
          return
        }

        // move made successfully, so save the thing
        await table.save()
        sendMessage(xmpp, userJID, message)

        // declare if trick was won
        if (trickWinner) {
          sendMessage(xmpp, userJID, `${trickWinner} gets the trick!`)
        }

        // Let the robot play
        // TODO: eventually remove.
        await botPlay(table, userJID)

        // display status again
        for (m of await table.showStatus(userJID)) {
          sendMessage(xmpp, userJID, m)
        }
        break
      }

      // force bot
      case 'bot':
      case 'botplay': {
        await botPlay(await getTableByHost(userJID), userJID)
        break
      }


      // a few fun extra reactions ;-)

      case 'hello':
      case 'hi':
      case 'hey':
      case 'sup':
      case 'yo':
      case 'wassup':
      case "what's": {
        var messages = [
          'Hi there!',
          "What's up?",
          'Hey, wassup?',
          'sup',
          'hi :)',
          'Hello',
        ]

        sendMessage(xmpp, userJID, messages[Math.floor(Math.random() * messages.length)])

        break
      }

      case 'ok':
      case 'okie':
      case 'wokay':
      case 'okay': {
        sendMessage(xmpp, userJID, '🙂')
        break
      }

      case 'delete': { // TODO: remove after debugging!
        if (command.length == 2 && command[0] == 'DELETE' && command[1] == 'ALL') {
          SingleTable.deleteMany({}, function (err, tables) {
            if (err) console.log(err)
            sendMessage(xmpp, userJID, 'Whoops! All tables deleted!!')
            sendMessage(xmpp, userJID, '/me hopes that was okay')
          })
        } else {
          sendMessage(xmpp, userJID, 'What??')
        }
        break
      }


      case 'good': {
        if (
          command.length == 2 &&
          ['morning', 'night'].includes(command[1].toLowerCase())
        ) {
          sendMessage(xmpp, userJID, `Good ${command[1]} to you too! 😁`)
          break
        }
      }

      default: {
        sendMessage(xmpp, userJID, 'Sorry, I didn\'t quite get that 🙁')
        sendMessage(xmpp, userJID, 'I need some help—or maybe it\'s you who needs it.')
        sendMessage(xmpp, userJID, 'Why don\'t you try typing "help"—then I can tell you what I can do')
      }
    }
  }
})

xmpp.on("online", async (address) => {
  // Makes itself available
  await xmpp.send(xml("presence", {},
    xml("show", {}, "chat"),
    xml("status", {}, "Ready to play? Come say hi :)")
  ))
  console.log("connected to xmpp server")
})

xmpp.start().catch(console.error)

// handle termination
process.once('SIGINT', async (code) => {
  console.log("Saying bye before signing off")
  await xmpp.send(xml("presence", {},
    xml("show", {}, "xa"),
    xml("status", {}, "I've been asked to terminate. Bye for now :(")
  ))
  await mongoose.connection.close()
  await xmpp.stop()
})
