#!/bin/sh
#
# Handles post-processing when the code is updated
# You might want to run this script via the 'post-receive' hook
# on your git server.
#
# The simplest way to do this would be to copy this file to
# .git/post-receive
#
# While you're about it, you might also want to update your
# 'push-to-checkout' hook to contain the following:
#
# echo "🐶 Updating working directory"
# git read-tree -u -m HEAD "$1"
#
# Don't forget to set the 'executable' permission on the file!

echo "
🐶 Rebuilding dependencies"
yarn install --force --non-interactive

echo "
🐶 Restarting application"
npm restart

echo "
🐶 Aaaand...we're ready to go 🎉"
